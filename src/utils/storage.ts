import { isElectron } from '@/utils'
import { Message } from '@arco-design/web-vue'

export interface DbDoc {
  _id: string
  _rev?: string
  [key: string]: any
}

export function setItem(key: string, value: any) {
  const data = JSON.stringify(value)

  if (isElectron) {
    return window.rubick.dbStorage.setItem(key, data)
  } else {
    localStorage.setItem(key, data)
  }
}

export function getItem(key: string) {
  if (isElectron) {
    const data = window.rubick.dbStorage.getItem(key)
    return data ? JSON.parse(data) : null
  } else {
    const data = localStorage.getItem(key)
    return data ? JSON.parse(data) : null
  }
}

export function removeItem(key: string) {
  const dbStorage = isElectron ? window.rubick.dbStorage : localStorage
  dbStorage.removeItem(key)
}

export function putDoc(doc: DbDoc) {
  if (!isElectron) {
    Message.error('当前环境暂不支持此功能')
    return
  }
  return window.rubick.db.put(doc)
}

export function getDoc(docId: string) {
  if (!isElectron) {
    Message.error('当前环境暂不支持此功能')
    return
  }
  return window.rubick.db.get(docId)
}

export function removeDoc(doc: DbDoc | string) {
  if (!isElectron) {
    Message.error('当前环境暂不支持此功能')
    return
  }
  return window.rubick.db.remove(doc)
}

export function allDocs(key?: string) {
  if (!isElectron) {
    Message.error('当前环境暂不支持此功能')
    return
  }
  return window.rubick.db.allDocs(key)
}

export function postAttachment(...args: any) {
  if (!isElectron) {
    Message.error('当前环境暂不支持此功能')
    return
  }
  // @ts-ignore
  return window.rubick.db.postAttachment(...args)
}

export function getAttachment(...args: any) {
  if (!isElectron) {
    Message.error('当前环境暂不支持此功能')
    return
  }
  // @ts-ignore
  return window.rubick.db.getAttachment(...args)
}
