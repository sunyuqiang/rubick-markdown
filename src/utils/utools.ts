import { isElectron } from './env'
import { Message } from '@arco-design/web-vue'

export function setFeature(feature: any) {
  if (!isElectron) return
  return window.rubick.setFeature(feature)
}

export function getFeatures(codes?: string[]): any {
  if (!isElectron) return
  // @ts-ignore
  return window.rubick.getFeatures(codes)
}

export function removeFeature(code: string) {
  if (!isElectron) return
  return window.rubick.removeFeature(code)
}

export function showOpenDialog(options: any): Promise<string[] | FileList | undefined> {
  if (isElectron) {
    return new Promise((resolve) => {
      const res = window.rubick.showOpenDialog(options)
      resolve(res || undefined)
    })
  } else {
    return new Promise((resolve) => {
      const isMultiple = !!options.properties?.includes('multiSelections')
      const input = document.createElement('input')
      input.type = 'file'
      input.multiple = isMultiple

      // TODO: 目前只接受图片 适用性较差
      input.accept =
        options.filters
          ?.map((item: any) => item.extensions.map((ext: any) => `image/${ext}`).join(','))
          .join(',') || ''

      input.click()
      input.onchange = () => {
        const files = input.files || undefined

        if (!files) return

        // 检查文件类型
        if (options.filters) {
          const extensions = options.filters
            .map((item: any) => item.extensions)
            .flat()
            .map((ext: any) => ext.toLowerCase())

          for (let i = 0; i < files.length; i++) {
            const file = files?.item(i)
            if (file && !extensions.includes(file.name.split('.').pop()?.toLowerCase() || '')) {
              Message.error('文件类型不符合要求')
              return
            }
          }
        }

        resolve(input.files || undefined)
      }
      input.onabort = () => {
        resolve(undefined)
      }
    })
  }
}

export async function screenCapture(): Promise<string> {
  if (!isElectron) {
    Message.error('当前环境不支持此操作')
    return ''
  }
  return new Promise((resolve) => {
    window.rubick.screenCapture((base64Str: any) => {
      resolve(base64Str)
    })
  })
}
