# SuperMarkdown

- ✅ 由ByteMD强力驱动，功能丰富、性能强劲
- ✅ 支持GFM扩展语法、脚注、Gemoji、KaTeX数学公式、Mermaid图表
- ✅ 支持通过Frontmatter设置多种主题、代码高亮样式
- ✅ 支持多级目录，目录支持无限嵌套
- ✅ 支持通过粘贴/拖拽的方式批量上传图片、支持截取屏幕截图
- ✅ 支持Markdown文件的批量导入、批量导出
- ✅ 支持插件多开，同时编辑/参考多个文章
- ✅ 支持实时同步预览、自动保存

![](https://pica.zhimg.com/80/v2-7e68c0ca4e0bf1492ffdd9548ee548c4_720w.png)